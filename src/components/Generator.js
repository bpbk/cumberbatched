import React, { Component } from 'react';
// import app from 'firebase';
// import 'firebase/auth';
// import 'firebase/database';
// import { firebaseConfig } from '../config'
import { b2, d1, c2, b1 } from '../data'

export default class Generator extends Component {
  constructor(props) {
    super(props)
    // if (!app.apps.length) {
    //   app.initializeApp(firebaseConfig);
    //   this.auth = app.auth();
    //   const wordsDb = app.database().ref('words')
    //   wordsDb.once("value")
    //   .then(function(snapshot) {
    //     console.log(snapshot)
    //     var key = snapshot.key; // "ada"
    //     var childKey = snapshot.child("name/last").key; // "last"
    //   });
    // }
    // console.log(app)
    //
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      update: true
    }

  }

  handleClick () {
    this.setState({
      update: !this.state.update
    })
  }

  render () {
    const word1 = b2[ b2.length * Math.random() << 0 ]
    const word2 = d1[ d1.length * Math.random() << 0 ]
    const word3 = c2[ c2.length * Math.random() << 0 ]
    const word4 = b1[ b1.length * Math.random() << 0 ]
    return (
      <div className="benedict-container">
        <div className="benedict-name-container">
          <div className="benedict-name">
            <p className="first-name">
              <span>{word1}</span>
              <span>{word2}</span>
            </p>
            <p className="last-name">
              <span>{word3}</span>
              <span>{word4}</span>
            </p>
          </div>
          <div className="benedict-button" onClick={this.handleClick}>DO IT AGAIN</div>
        </div>
      </div>
    )
  }
}
