import React from 'react';
import Generator from './Generator'
const benedict = require('../images/benedict.jpg')

function App() {
  return (
    <div className="App" style={{backgroundImage: `url(${benedict})`}}>
      <Generator/>
    </div>
  );
}

export default App;
